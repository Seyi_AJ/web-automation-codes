/**
 * 
 */
package com.myStrore.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myStore.actiondriver.Action;
import com.myStore.base.BaseClass;

/**
 * @author USER
 *
 */
public class AccountCreationPage extends BaseClass{
	Action action = new Action(driver);
	
	@FindBy (xpath= "//h1[text()='Create an account']")
    WebElement formTile;
	
	public AccountCreationPage() {
		PageFactory.initElements(driver, this);
	}
	
	public boolean validateAccountCreationPage() {
	return	action.isDisplayed(driver, formTile);
	}
}
