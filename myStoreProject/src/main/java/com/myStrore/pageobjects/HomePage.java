/**
 * 
 */
package com.myStrore.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myStore.actiondriver.Action;
import com.myStore.base.BaseClass;

/**
 * @author USER
 *
 */
public class HomePage extends BaseClass {
	Action action= new Action(driver);
	
	@FindBy(xpath="//span[text()='My Wishlists']")
	WebElement myWishlist;

	@FindBy(xpath="//span[text()='Order history and details']")
	WebElement orderHistory;
	
	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	
	public boolean validateMyWishList () {
		action.fluentWait(driver, myWishlist, 10);
	return	action.isDisplayed(driver, myWishlist);
		
	}
	
	public String getCurrentURL () {
		String getHomePageURL = driver.getCurrentUrl();
		return getHomePageURL;
	}
	
	
    public boolean validateOrderHistory() {
    	return action.isDisplayed(driver, orderHistory);
    }
}
