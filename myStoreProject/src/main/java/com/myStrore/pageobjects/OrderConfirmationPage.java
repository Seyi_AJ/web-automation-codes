/**
 * 
 */
package com.myStrore.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myStore.actiondriver.Action;
import com.myStore.base.BaseClass;

/**
 * @author USER
 *
 */
public class OrderConfirmationPage extends BaseClass {
	Action action = new Action(driver);

	@FindBy(xpath="//p/strong[contains(text(),'Your order on my store is complete.')]")
	WebElement confirmMessage;
	
	public OrderConfirmationPage () {
		PageFactory.initElements(driver, this);
	}
	
	public String validateConfirmMessage () {
		String confirmMsg = confirmMessage.getText();
		return confirmMsg;
	}
}
