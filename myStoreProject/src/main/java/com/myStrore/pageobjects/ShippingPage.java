/**
 * 
 */
package com.myStrore.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myStore.actiondriver.Action;
import com.myStore.base.BaseClass;

/**
 * @author USER
 *
 */
public class ShippingPage  extends BaseClass {
	Action action = new Action(driver);
	
	@FindBy(id="cgv")
    WebElement terms;
	
	@FindBy(xpath="//button/span[contains(text(),'Proceed to checkout')]")
    WebElement proceedToCheckoutBtn;
	
	public ShippingPage () {
		PageFactory.initElements(driver, this);
	}
	
	public void checkTheTerms () {
		action.click(driver, terms);
	}
	
	
	public PaymentPage proceedToCheckOut () {
		action.click(driver, proceedToCheckoutBtn);
		return new PaymentPage ();
	}
}
