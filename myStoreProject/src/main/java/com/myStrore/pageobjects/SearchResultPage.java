/**
 * 
 */
package com.myStrore.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myStore.actiondriver.Action;
import com.myStore.base.BaseClass;

/**
 * @author USER
 *
 */
public class SearchResultPage extends BaseClass{
	Action action = new Action(driver);
	

	@FindBy(xpath="//*[@id=\"center_column\"]//img")
	WebElement productResult;
	
	public SearchResultPage () {
		PageFactory.initElements(driver, this);
	}
     
	public boolean isProductAvailable () {
		action.fluentWait(driver, productResult, 10);
	return	action.isDisplayed(driver, productResult);
		
	}
	
	public AddToCartPage clickOnProduct() {
		action.fluentWait(driver, productResult, 10);
		action.click(driver, productResult);
		return new AddToCartPage();
		
	}
}
