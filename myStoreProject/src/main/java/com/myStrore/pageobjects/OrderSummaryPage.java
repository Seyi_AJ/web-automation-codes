/**
 * 
 */
package com.myStrore.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myStore.actiondriver.Action;
import com.myStore.base.BaseClass;

/**
 * @author USER
 *
 */
public class OrderSummaryPage extends BaseClass {
	Action action = new Action(driver);
	
	@FindBy(xpath="//span[contains(text(),'I confirm my order')]")
	WebElement confirmOrderBtn;
	
	public OrderSummaryPage () {
		PageFactory.initElements(driver, this);
	}

	
	public OrderConfirmationPage clickOnConfirmOrderBtn () {
		action.click(driver, confirmOrderBtn);
		return new OrderConfirmationPage();
	}
}
