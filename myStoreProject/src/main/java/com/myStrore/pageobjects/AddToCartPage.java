/**
 * 
 */
package com.myStrore.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myStore.actiondriver.Action;
import com.myStore.base.BaseClass;

/**
 * @author USER
 *
 */
public class AddToCartPage extends BaseClass {
	Action action = new Action(driver);

	
	@FindBy(id="quantity_wanted")
	WebElement quantity;
	
	@FindBy(name="group_1")
	WebElement size;
	
	@FindBy(xpath="//span[text()='Add To Cart']")
	WebElement addToCartBtn;
	
	@FindBy(xpath="//*[@id=\"layer_cart\"]//h2/i")
	WebElement addToCartMessage;
	
	@FindBy(xpath="//span[contains(text(),'Proceed to checkout')]")
	WebElement proceedToCheckOutBtn;
	
	public AddToCartPage () {
		PageFactory.initElements(driver, this);
	}
	
	public void enterQuantity(String quantity1) {
		action.type(quantity, quantity1);
	}
	public void selectSize(String size1) {
		action.selectByVisibleText(size1, size);
		
	}
	
	public void clickOnAddToCart () {
		action.click(driver, addToCartBtn);
		
	}
	
	public boolean validateAddToCart() {
		action.fluentWait(driver, addToCartMessage, 10);
	return	action.isDisplayed(driver, addToCartMessage);
	}
	public OrderPage clickOnCheckOut () throws Exception {
		action.fluentWait(driver, proceedToCheckOutBtn, 10);
		action.JSClick(driver, proceedToCheckOutBtn);
		return new OrderPage();
	}
	
}
