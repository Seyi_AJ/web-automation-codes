package com.myStrore.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myStore.actiondriver.Action;
import com.myStore.base.BaseClass;

public class IndexPage extends BaseClass {
	Action action= new Action(driver);
	
	@FindBy(xpath="//a[@class='login']")
	WebElement signInBtn;
	
	@FindBy (xpath="//img[@class='logo img-responsive']")
	WebElement myStorelogo;
	
	@FindBy (id="search_query_top")
	WebElement searchProductbox;
	
	@FindBy (name="submit_search")
	WebElement searchButton;
	
	public IndexPage () {
		PageFactory.initElements(driver, this);
	}
	
	public LoginPage clickSignInBtn () {
		action.explicitWait(driver, signInBtn, 10);
		action.click(driver, signInBtn);
		return new LoginPage();
	}
	
	public boolean validateLogo() {	
	return action.isDisplayed(driver, myStorelogo);
	
		
	}
	
	public String getMyStoreTitle () {
		String myStoreTitle = driver.getTitle();
		return myStoreTitle;
	}
	
	public SearchResultPage searchProduct (String productName ) {
		action.type(searchProductbox, productName );
		action.click(driver, searchButton);
		return new SearchResultPage();
		
	}
	
}
