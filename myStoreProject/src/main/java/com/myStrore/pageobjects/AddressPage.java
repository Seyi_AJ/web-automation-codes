/**
 * 
 */
package com.myStrore.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myStore.actiondriver.Action;
import com.myStore.base.BaseClass;

/**
 * @author USER
 *
 */
public class AddressPage extends BaseClass {
	Action action = new Action(driver);
	
	@FindBy(xpath="//span[text()='Proceed to checkout']")
	WebElement proceedToCheckout;
	
	public AddressPage () {
		PageFactory.initElements(driver, this);
	}
    
	public ShippingPage clickOnCheckout () {
		action.click(driver, proceedToCheckout);
		return new ShippingPage();
	}
}
