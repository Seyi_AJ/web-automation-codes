package com.myStrore.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myStore.actiondriver.Action;
import com.myStore.base.BaseClass;

public class LoginPage extends BaseClass{
	
	Action action= new Action(driver);
	
	@FindBy(id="email")
	WebElement userName;

	@FindBy(name="passwd")
	WebElement password;
	
	@FindBy(xpath="//button[@id='SubmitLogin']")
	WebElement signinBtn;
	
	@FindBy(name="email_create")
	WebElement emailForNewAccount;
	
	@FindBy(name="SubmitCreate")
	WebElement createNewAccountBtn;
	
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	
	public HomePage login(String uname, String passwd) {
		action.type(userName, uname);
		action.type(password, passwd);
		action.explicitWait(driver, signinBtn, 10);
		action.click(driver, signinBtn);
		return new HomePage();	
	}
	
	public AddressPage login1 (String uname, String passwd) {
		action.type(userName, uname);
		action.type(password, passwd);
		action.click(driver, signinBtn);
		return new AddressPage();	
	}
	
	
	public AccountCreationPage createNewAccount (String newEmail) {
		action.type(emailForNewAccount, newEmail);
		action.click(driver, createNewAccountBtn);
		return new AccountCreationPage();
		
	}
}
