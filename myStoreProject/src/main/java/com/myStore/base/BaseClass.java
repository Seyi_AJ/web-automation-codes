package com.myStore.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import org.apache.commons.logging.Log;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;

import com.myStore.actiondriver.Action;
import com.myStore.utlities.ExtentManager;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseClass {
	public static Properties prop; 
	public static WebDriver driver;
	public static Action action;
	public static Logger Log = LogManager.getLogger(Log.class.getName());
	
	
	@BeforeSuite(groups= {"Smoke", "Sanity", "Regression"})
	public void loadConfig () throws Exception {
		ExtentManager.setExtent();
		try {
			File src = new File ("Configuration\\Config.properties");
			prop = new Properties();
			System.out.println("Super constructor invoked");
			FileInputStream fis  = new FileInputStream(src);
			prop.load(fis);
			System.out.println("driver: " + driver);
		} catch (FileNotFoundException e)  {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		     
}
	
	public void launchApp (String browserName) {
		WebDriverManager.chromedriver().setup();
		WebDriverManager.firefoxdriver().setup();
		//String browserName = prop.getProperty("browser");
		if (browserName.contains("Chrome")) {
			driver = new ChromeDriver ();
		} else if (browserName.contains("Firefox")) {
			driver = new FirefoxDriver();
		}
		
		driver.manage().window().maximize();
	    driver.manage().deleteAllCookies();
	    action.implicitWait(driver, 10);
	    driver.get(prop.getProperty("baseURL"));
	    
	}
	
	@AfterSuite
	public void afterSuite () {
		ExtentManager.endReport();
	}
	
	@AfterTest
	public void tearDown() {
		driver.quit();
	}

	
	
	}
