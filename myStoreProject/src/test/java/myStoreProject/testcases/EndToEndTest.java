/**
 * 
 */
package myStoreProject.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.myStore.base.BaseClass;
import com.myStrore.pageobjects.AddToCartPage;
import com.myStrore.pageobjects.AddressPage;
import com.myStrore.pageobjects.IndexPage;
import com.myStrore.pageobjects.LoginPage;
import com.myStrore.pageobjects.OrderConfirmationPage;
import com.myStrore.pageobjects.OrderPage;
import com.myStrore.pageobjects.OrderSummaryPage;
import com.myStrore.pageobjects.PaymentPage;
import com.myStrore.pageobjects.SearchResultPage;
import com.myStrore.pageobjects.ShippingPage;

/**
 * @author USER
 *
 */
public class EndToEndTest extends BaseClass {
	
	IndexPage indexPage;
	SearchResultPage searchResultPage;
	AddToCartPage addToCartPage;
	OrderPage orderPage;
	LoginPage loginPage;
	AddressPage addressPage;
	ShippingPage shippingPage;
	PaymentPage paymentPage;
	OrderSummaryPage orderSummaryPage;
	OrderConfirmationPage orderConfirmationPage;
	
	@Parameters("browser")
	@BeforeMethod(groups = {"Smoke", "Sanity", "Regression"})
	public void setUp (String browser) {
		launchApp(browser);
	}
	
	@Test(groups = "Regression")
	public void endToEndTest () throws Throwable {
		indexPage = new IndexPage();
		searchResultPage = indexPage.searchProduct("t-shirt");
		addToCartPage = searchResultPage.clickOnProduct();
		addToCartPage.enterQuantity("2");
		addToCartPage.selectSize("M");
		addToCartPage.clickOnAddToCart();
		orderPage = addToCartPage.clickOnCheckOut();
		loginPage = orderPage.clickOnCheckout();
		addressPage = loginPage.login1(prop.getProperty("username"), prop.getProperty("password"));
		shippingPage = addressPage.clickOnCheckout();
		shippingPage.checkTheTerms();
		paymentPage = shippingPage.proceedToCheckOut();
		orderSummaryPage = paymentPage.clickOnPaymentMethod();
		orderConfirmationPage = orderSummaryPage.clickOnConfirmOrderBtn();
		String actualMessage = orderConfirmationPage.validateConfirmMessage();		
		String expectedMessage = "Your order on my store is complete";
		Assert.assertEquals(actualMessage, expectedMessage);
			
	}
	
	
	@AfterMethod(groups = {"Smoke", "Sanity", "Regression"})
	public void tearDown () {
		driver.quit();
	}


}
