/**
 * 
 */
package myStoreProject.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.myStore.base.BaseClass;
import com.myStore.utlities.NewExcelLibrary;
import com.myStrore.pageobjects.AddToCartPage;
import com.myStrore.pageobjects.IndexPage;
import com.myStrore.pageobjects.OrderPage;
import com.myStrore.pageobjects.SearchResultPage;

/**
 * @author USER
 *
 */
public class OrderPageTest extends BaseClass {
	IndexPage indexPage;
	SearchResultPage searchResultPage;
	AddToCartPage addToCartPage;
	OrderPage orderPage;
	
	@Parameters("browser")
	@BeforeMethod(groups = {"Smoke", "Sanity", "Regression"})
	public void setUp (String browser) {
		launchApp(browser);
	}
	
	@Test(dataProvider = "getProductData", dataProviderClass = NewExcelLibrary.class, groups = "Regression")
	public void verifyTotalPrice (String Product, String QTY, String Size ) throws Throwable {
		Log.info("Start Test case");
		Log.info("Verify Total Price");
		indexPage = new IndexPage();
		searchResultPage = indexPage.searchProduct(Product);
		addToCartPage = searchResultPage.clickOnProduct();
		addToCartPage.enterQuantity(QTY);
		addToCartPage.selectSize(Size);
		addToCartPage.clickOnAddToCart();
		orderPage = addToCartPage.clickOnCheckOut();
		Double unitPrice = orderPage.getUnitPrice();
		Double totalPrice = orderPage.getTotalPrice();
		Double totalExpectedPrice = (unitPrice*(Double.parseDouble(QTY)))+2;
		Assert.assertEquals(totalPrice, totalExpectedPrice);
		Log.info("Verify Total Price");
		Log.info("End Test Case");
			
	}
	
	
	@AfterMethod(groups = {"Smoke", "Sanity", "Regression"})
	public void tearDown () {
		driver.quit();
	}

}
