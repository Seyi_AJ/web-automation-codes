/**
 * 
 */
package myStoreProject.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.myStore.base.BaseClass;
import com.myStrore.pageobjects.IndexPage;

/**
 * @author USER
 *
 */
public class IndexPageTest extends BaseClass {
	IndexPage indexPage;
	
	
	@Parameters("browser")
	@BeforeTest(groups = {"Smoke", "Sanity", "Regression"})
	public void setUp (String browser) {
		launchApp(browser);
	}

	
	
	@Test(groups = "Smoke")
	public void verifyLogo () {
		Log.info("Start verifyLogo Test");
		indexPage = new IndexPage ();
       boolean result = indexPage.validateLogo();
		Assert.assertTrue(result);
		Log.info("End verifyLogo Test");
		
	}
	@Test(groups = "Smoke")
	public void verifyTitle () {
		
		String actTitle = indexPage.getMyStoreTitle();
		Assert.assertEquals(actTitle, "My Store");
		Log.info("End verify Title Test");
	}
	@AfterTest(groups = {"Smoke", "Sanity", "Regression"})
	public void closeBrowser() {
		driver.quit();
	}
}
