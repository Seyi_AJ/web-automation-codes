/**
 * 
 */
package myStoreProject.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.myStore.base.BaseClass;
import com.myStore.utlities.NewExcelLibrary;
import com.myStrore.pageobjects.AddToCartPage;
import com.myStrore.pageobjects.IndexPage;
import com.myStrore.pageobjects.SearchResultPage;

/**
 * @author USER
 *
 */
public class AddToCartPageTest extends BaseClass {
	IndexPage indexPage;
	SearchResultPage searchResultPage;
	AddToCartPage addToCartPage;
	
	
	
	@Parameters("browser")
	@BeforeMethod(groups = {"Smoke", "Sanity", "Regression"})
	public void setUp (String browser) {
		launchApp(browser);
	}
	
	@Test(dataProvider = "getProductData", dataProviderClass = NewExcelLibrary.class, groups = {"Sanity", "Regression"})
	public void addToCartTest (String Product, String QTY, String Size) {
		indexPage = new IndexPage();
		searchResultPage = indexPage.searchProduct(Product);
		addToCartPage = searchResultPage.clickOnProduct();
		addToCartPage.enterQuantity(QTY);
		addToCartPage.selectSize(Size);
		addToCartPage.clickOnAddToCart();
		boolean result = addToCartPage.validateAddToCart();
		Assert.assertTrue(result);
			
	}
	
	
	@AfterMethod(groups = {"Smoke", "Sanity", "Regression"})
	public void tearDown () {
		driver.quit();
	}

}
