/**
 * 
 */
package myStoreProject.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.myStore.base.BaseClass;
import com.myStore.utlities.NewExcelLibrary;
import com.myStrore.pageobjects.HomePage;
import com.myStrore.pageobjects.IndexPage;
import com.myStrore.pageobjects.LoginPage;

/**
 * @author USER
 *
 */
public class LoginPageTest extends BaseClass {
IndexPage indexPage;
LoginPage loginPage;
HomePage homePage;


@Parameters("browser")
	@BeforeMethod(groups = {"Smoke", "Sanity", "Regression"})
	public void setUp (String browser) {
	
		launchApp(browser);
	}
	@Test(dataProvider = "loginData", dataProviderClass = NewExcelLibrary.class, groups = {"Smoke","Sanity"})
	public void loginTest (String uname, String passwd) {
		Log.info("Start Login Test");
		indexPage = new IndexPage();
		Log.info("User is going to click on Signin");
		loginPage = indexPage.clickSignInBtn();
		Log.info("Enter Username and Password");
		homePage = loginPage.login(uname, passwd);
		String actualURL = homePage.getCurrentURL();
		String expectedURL = "http://automationpractice.com/index.php?controller=my-account";
		Log.info("Verifying if user is able to Login");
		Assert.assertEquals(actualURL, expectedURL);
		Log.info("Login is successful");
		Log.info("End of Login Test");
		
		
		
	}
	@AfterMethod(groups = {"Smoke", "Sanity", "Regression"})
	public void closeBrowser() {
		driver.quit();
	}
}

