/**
 * 
 */
package myStoreProject.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.myStore.base.BaseClass;
import com.myStrore.pageobjects.HomePage;
import com.myStrore.pageobjects.IndexPage;
import com.myStrore.pageobjects.LoginPage;

/**
 * @author USER
 *
 */
public class HomePageTest extends BaseClass {
	IndexPage indexPage;
	LoginPage loginPage;
	HomePage homePage;

	@Parameters("browser")
	@BeforeMethod(groups = {"Smoke", "Sanity", "Regression"})
	public void setUp (String browser) {
		launchApp(browser);
	}
	@Test(groups = "Smoke")
	public void wishListTest () {
		Log.info("Start HomePage Test");
		indexPage = new IndexPage();
		loginPage = indexPage.clickSignInBtn();
		Log.info("Enter Username and Password");
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		boolean result = homePage.validateMyWishList();
		Assert.assertTrue(result);
		Log.info("End HomePage Test");
		
	}
	@Test(groups="Smoke")
	public void orderHistoryAndDetailsTest () {
		indexPage = new IndexPage();
		loginPage = indexPage.clickSignInBtn();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		boolean result = homePage.validateOrderHistory();
		Assert.assertTrue(result);
		
		
	}
	
	
	
	
	@AfterMethod(groups = {"Smoke", "Sanity", "Regression"})
	public void tearDown () {
		driver.quit();
	}
}
