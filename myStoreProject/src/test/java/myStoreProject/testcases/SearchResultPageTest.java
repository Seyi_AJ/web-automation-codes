/**
 * 
 */
package myStoreProject.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.myStore.base.BaseClass;
import com.myStore.utlities.NewExcelLibrary;
import com.myStrore.pageobjects.IndexPage;
import com.myStrore.pageobjects.SearchResultPage;

/**
 * @author USER
 *
 */
public class SearchResultPageTest extends BaseClass {
	IndexPage indexPage;
	SearchResultPage searchResultPage;
	
	
	@Parameters("browser")
	@BeforeMethod(groups = {"Smoke", "Sanity", "Regression"})
	public void setUp (String browser) {
		launchApp(browser);
	}
	
	@Test(dataProvider ="searchProduct", dataProviderClass = NewExcelLibrary.class, groups = "Smoke")
	public void productAvailabilityTest (String Product) {
		Log.info("Start productAvailabilityTest");
		indexPage = new IndexPage();
		searchResultPage = indexPage.searchProduct(Product);
		boolean result = searchResultPage.isProductAvailable();
		Assert.assertTrue(result);
		Log.info("End productAvailabilityTest");
	}
	
	
	@AfterMethod(groups = {"Smoke", "Sanity", "Regression"})
	public void tearDown () {
		driver.quit();
	}

}
