/**
 * 
 */
package myStoreProject.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.myStore.base.BaseClass;
import com.myStore.utlities.NewExcelLibrary;
import com.myStrore.pageobjects.AccountCreationPage;
import com.myStrore.pageobjects.IndexPage;
import com.myStrore.pageobjects.LoginPage;

/**
 * @author USER
 *
 */
public class AccountCreationPageTest extends BaseClass{
	IndexPage indexPage;
	LoginPage loginPage;
	AccountCreationPage accountCreationPage;
	
	@Parameters("browser")
	@BeforeTest(groups = {"Smoke", "Sanity", "Regression"})
	public void setUp (String browser) {
		launchApp(browser);
	}

	@Test(dataProvider = "emailData", dataProviderClass = NewExcelLibrary.class, groups= "Sanity")
	public void verifyAccountCreationPageTest (String email) {
		Log.info("Start AccountCreationPageTest");
		indexPage = new IndexPage ();
		loginPage = indexPage.clickSignInBtn();
		accountCreationPage = loginPage.createNewAccount(email);
		boolean result = accountCreationPage.validateAccountCreationPage();		
		Assert.assertTrue(result);
		Log.info("End AccountCreationPage Test");
	}
	
	
	
	@AfterTest(groups = {"Smoke", "Sanity", "Regression"})
	public void tearDown() {
		driver.quit();
	}
}
