package com.parabank.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.parabank.base.BaseClass;
import com.parabank.utils.TestData;

public class LoginTests extends BaseClass {
	

	@Test(dataProvider = "loginData", dataProviderClass = TestData.class)
    public void loginTest(String username, String password, String expectedResult) {
		
        loginPage.enterEmail(username);
        loginPage.enterPassword(password);
        loginPage.clickLoginButton();

        String pageTitle = loginPage.getPageTitle();
        String actualResult = null;

        if (pageTitle.contains("Accounts Overview")) {
            actualResult = "Accounts Overview";
        } else if (pageTitle.contains("Error")) {
            actualResult = "The username and password could not be verified.";
        } else {
            throw new IllegalStateException("Unexpected page title: " + pageTitle);
        }

        Assert.assertEquals(actualResult, expectedResult, "The login result does not match the expected result.");
        logger.info(String.format("Logging in with username and password", expectedResult, actualResult));
    }
    
}
