package com.parabank.base;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import com.parabank.pages.LoginPage;
import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseClass {
    protected WebDriver driver;
    protected LoginPage loginPage;
    protected  Logger logger = LogManager.getLogger(BaseClass.class);

	@BeforeMethod
public void setUp() {
        Properties config = new Properties();
        try {
            config.load(new FileInputStream("Configurations/config.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String browser = config.getProperty("browser");
        if(browser.equalsIgnoreCase("chrome")) {
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("--remote-allow-origins=*","ignore-certificate-errors");
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver(chromeOptions);
        } else if(browser.equalsIgnoreCase("firefox")) {
            FirefoxOptions firefoxOptions = new FirefoxOptions();
            firefoxOptions.addArguments("--remote-allow-origins=*","ignore-certificate-errors");
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver(firefoxOptions);
        } else if(browser.equalsIgnoreCase("edge")) {
            WebDriverManager.edgedriver().setup();
            driver = new EdgeDriver();
        } else {
            throw new IllegalArgumentException("Invalid browser specified in config.properties file");
        }

        driver.manage().window().maximize();
        driver.get(config.getProperty("URL"));
        loginPage = new LoginPage(driver);
        logger.info("Setting up the browser and navigating to the Parabank login page");
    }
    
    @AfterMethod
    public void tearDown() {
        driver.quit();
        logger.info("Quitting the browser");
    }
}
