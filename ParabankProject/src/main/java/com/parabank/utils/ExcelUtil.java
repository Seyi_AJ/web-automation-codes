package com.parabank.utils;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ExcelUtil {

    public static Object[][] readExcelData(String filePath, String sheetName) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(new File(filePath));
        Workbook workbook = new XSSFWorkbook(fileInputStream);
        Sheet sheet = workbook.getSheet(sheetName);
        Iterator<Row> rows = sheet.iterator();
        List<Object[]> rowData = new ArrayList<>();

        // Skip the header row
        if (rows.hasNext()) {
            rows.next();
        }

        while (rows.hasNext()) {
            Row currentRow = rows.next();
            Iterator<Cell> cells = currentRow.iterator();
            List<Object> cellData = new ArrayList<>();

            while (cells.hasNext()) {
                Cell currentCell = cells.next();
                if (currentCell.getCellType() == CellType.STRING) {
                    cellData.add(currentCell.getStringCellValue());
                } else if (currentCell.getCellType() == CellType.NUMERIC) {
                    cellData.add(currentCell.getNumericCellValue());
                }
            }
            rowData.add(cellData.toArray());
        }

        workbook.close();
        return rowData.toArray(new Object[0][]);
    }
}
