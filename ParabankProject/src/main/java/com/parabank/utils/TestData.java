package com.parabank.utils;

import java.io.IOException;
import org.testng.annotations.DataProvider;

public class TestData {

private static final String TEST_DATA_FILE = "src/test/resources/testdata/login-test-data.xlsx";

    @DataProvider(name = "loginData")
 public Object[][] getLoginData() throws IOException {
 return ExcelUtil.readExcelData(TEST_DATA_FILE, "Sheet1");
    }

}
