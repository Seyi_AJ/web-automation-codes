
package com.parabank.pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.parabank.base.BaseClass;

public class LoginPage extends BaseClass {

    @FindBy(xpath = "//input[@name='username']")
    private WebElement usernameInput;

    @FindBy(xpath = "//input[@name='password']")
    private WebElement passwordInput;

    @FindBy(xpath = "//input[@value='Log In']")
    private WebElement loginButton;
    
    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void enterEmail(String Username) {
        usernameInput.sendKeys(Username);
    }

    public void enterPassword(String Password) {
        passwordInput.sendKeys(Password);
    }

    public void clickLoginButton() {
        loginButton.click();
    }

    public String getPageTitle() {
        return driver.getTitle();
    }
}
