package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Homepage extends Basepage {

	private By clickOnFormAuth = By.xpath("/html/body/div[2]/div/ul/li[21]/a");
	
	public Homepage(WebDriver driver) {
		super(driver);
	
	}

	public void clickFormAuthentication () {
		driver.findElement(clickOnFormAuth).click();
	}
}
