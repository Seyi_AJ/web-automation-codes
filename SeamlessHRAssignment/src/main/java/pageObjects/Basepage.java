package pageObjects;

import org.openqa.selenium.WebDriver;

public class Basepage {
	
protected WebDriver driver;
	
	public Basepage (WebDriver driver) {
		this.driver = driver;	
		}
		
		
		public void setUpBrowser () {
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			driver.navigate().to("https://the-internet.herokuapp.com/");
		}
		
		
		public void closeBrowser () {
			driver.quit();
		}
}


