package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LogInPage extends Basepage {
	
	private By enterUsernameField = By.id("username");
	private By enterPasswordField = By.id("password");
	private By clickLogInButton = By.xpath("/html/body/div[2]/div/div/form/button/i");

	public LogInPage(WebDriver driver) {
		super(driver);
		
	}

	public void enterValidLogInDetails (String username, String password) {
		driver.findElement(enterUsernameField).sendKeys(username);
		driver.findElement(enterPasswordField).sendKeys(password);
		driver.findElement(clickLogInButton).click();
	}
	
	public void enterInvalidLogInDetails (String username, String password) {
		driver.findElement(enterUsernameField).sendKeys(username);
		driver.findElement(enterPasswordField).sendKeys(password);
		driver.findElement(clickLogInButton).click();
	}
}
