package pageObjects;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class GeneralHooksPage {
	protected Basepage basepage;
	protected Homepage homepage;
	protected Landingpage landingpage;
	protected LogInPage loginpage;
	
	

	@Before
	public void setUp() {	
	System.setProperty("webdriver.chrome.driver","C:\\Users\\USER\\Downloads\\Compressed\\chromedriver.exe");
	 WebDriver driver = new ChromeDriver();	
	this.basepage = new Basepage(driver);
	this.homepage = new Homepage(driver);
	this.landingpage = new Landingpage(driver);
	this.loginpage = new LogInPage(driver);
	basepage.setUpBrowser();
	
	
}
	@After
	public void tearDownBrowser() {
		basepage.closeBrowser();
	}
}


