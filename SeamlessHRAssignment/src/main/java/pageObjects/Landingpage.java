package pageObjects;

import org.openqa.selenium.WebDriver;

public class Landingpage extends Basepage {

	public Landingpage(WebDriver driver) {
		super(driver);
		
	}

	public boolean validateLogInTest (String wordsToValidate) {
		boolean result = driver.getPageSource().toLowerCase().contains(wordsToValidate);
		return result;
	}
	
	public boolean validateInvalidLogInTest (String wordsToValidate) {
		boolean result = driver.getPageSource().toLowerCase().contains(wordsToValidate);
		return result;
	}
}
