package testExecutionArea;

import org.junit.Test;

import pageObjects.GeneralHooksPage;

public class LogInTest extends GeneralHooksPage {

	@Test
	public void logInTest () {
		homepage.clickFormAuthentication();
		
		loginpage.enterValidLogInDetails("tomsmith", "SuperSecretPassword!");
		
		landingpage.validateLogInTest("You logged into a secure area!");
	}
}
