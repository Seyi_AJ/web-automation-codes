package testExecutionArea;

import org.junit.Test;

import pageObjects.GeneralHooksPage;

public class InvalidLogInTest extends GeneralHooksPage {

	
	@Test
	public void invalidLogInTest () {
		
		homepage.clickFormAuthentication();
		
		loginpage.enterInvalidLogInDetails("thomas", "SecretPassword!");
		
		landingpage.validateInvalidLogInTest("Your username is invalid!");
	}
}
